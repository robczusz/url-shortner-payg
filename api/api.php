<?php
/**
 * Created by PhpStorm.
 * User: robert
 * Date: 07/10/2018
 * Time: 13:44
 */

class api
{
    private $request;
    private $data = [];

    public function __construct() {

        $this->request = $_SERVER['REQUEST_METHOD'];

        if ('POST' == $this->request) {
            $data["short_url"] = '/abc123';
            // Let's decode Request Payload to variable
            $rqdata = json_decode( file_get_contents( 'php://input' ), true );
            $data["url"] = $rqdata["url"];

            $this->returnJson($data);
        } elseif  ('GET' == $this->request) {
            $this->redirectToUrl('http://www.payasugym.com');
        } else {
            return false;
        }
    }

    /**
    * @param $url
    */
    private function redirectToUrl($url) {

        header("Location: " . $url, true, 301);
        exit();
    }

    /**
     * @param $url
     */
    private function returnJson($data) {

        header('Content-type: application/json');
        echo json_encode($data);

    }


}

new api();

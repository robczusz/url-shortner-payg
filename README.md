# Technical Test PAYG  - Robert Czusz - PHP Developer

I used no framework to complete the task therefore the api is written very 
basic way and __constructor is mainly used for action

My environment for development was:

```
PHP 7.2.5
NGINX 1.13.12
npm 6.4.1
```

## Setup

Install composer and node.js/npm then 

```bash
composer install
npm install
npm run watch
```

## Env setup - NGINX (nginx.conf)

```bash
worker_processes  1;

events {
    worker_connections  1024;
}


http {
    include       mime.types;
    default_type  application/octet-stream;

    sendfile        on;

    keepalive_timeout  65;

    server {
        listen       80;
        server_name  localhost;

        location / {
            # No mistake - must be no space before 200 
            error_page 405 =200 $uri;
            
            # to be changed root path
            root   /Users/robert/code/urlshortner-test;
            index  index.html index.htm;
        }

        location ~ ^/abc123 {
            try_files $uri $uri/ /api/api.php;
        }

        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }

        location ~ \.php$ {
            # to be changed root path
            root           /Users/robert/code/urlshortner-test;
            fastcgi_pass   127.0.0.1:9000;
            fastcgi_index  index.php;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            include        fastcgi_params;
        }

    }

    include servers/*;
}

```

## SCSS compiled with sass

```bash
sass --watch src/sass/app.scss:style.css

```

## TODO

 - use framework
 - create proper REST api server
 - unittest your classes once created
 - style it to look pretty
 - implement proper react form validation


# Below - original spec from repository


Without using an external database, we'd like you to create a URL shortening service. 

There should be an endpoint that responds to POST with a json body containing a URL, which responds with a JSON repsonse of the short url and the orignal URL, as in the following curl example:

~~~
curl localhost -XPOST -d '{ "url": "http://www.payasugym.com" }'
{ "short_url": "/abc123", "url": "http://www.payasugym.com" }
~~~
When you send a GET request to a previously returned URL, it should redirect to the POSTed URL, as shown in the following curl example:

~~~
curl -v localhost/abc123
...
< HTTP/1.1 301 Moved Permanently
...
< Location: http://www.payasugym.com
...
{ "url": "http://www.payasugym.com" }
~~~
On the frontend we would like you to use React, there should be an input field and submit button on the page into which the URL you would like to shorten can be entered and the short url should be returned 

To start you off on the frontend we have created a hello world example, which you can use as the basis for the React Frontend.
To start off, clone the repository and create a new branch.

To install the node modules run:
~~~
npm install
~~~

To create the JS bundle and run the watcher run:
~~~
npm run watch
~~~

You can use whatever npm modules and PHP framework you are comfortable with. 

For extra points: Style the frontend with Sass

Please don't spend more than a few hours on this.

## Submission

Please clone this repository, write your code and update this README with a guide of how to run it.

Either send us a link to the repository on somewhere like github or bitbucket (bitbucket has free private repositories).


# Url Shortner

Without using an external database, we'd like you to create a URL shortening service. 

There should be an endpoint that responds to POST with a json body containing a URL, which responds with a JSON repsonse of the short url and the orignal URL, as in the following curl example:

~~~
curl localhost -XPOST -d '{ "url": "http://www.payasugym.com" }'
{ "short_url": "/abc123", "url": "http://www.payasugym.com" }
~~~
When you send a GET request to a previously returned URL, it should redirect to the POSTed URL, as shown in the following curl example:

~~~
curl -v localhost/abc123
...
< HTTP/1.1 301 Moved Permanently
...
< Location: http://www.payasugym.com
...
{ "url": "http://www.payasugym.com" }
~~~
On the frontend we would like you to use React, there should be an input field and submit button on the page into which the URL you would like to shorten can be entered and the short url should be returned 

To start you off on the frontend we have created a hello world example, which you can use as the basis for the React Frontend.
To start off, clone the repository and create a new branch.

To install the node modules run:
~~~
npm install
~~~

To create the JS bundle and run the watcher run:
~~~
npm run watch
~~~

You can use whatever npm modules and PHP framework you are comfortable with. 

For extra points: Style the frontend with Sass

Please don't spend more than a few hours on this.

## Submission

Please clone this repository, write your code and update this README with a guide of how to run it.

Either send us a link to the repository on somewhere like github or bitbucket (bitbucket has free private repositories).


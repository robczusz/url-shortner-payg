import request from "superagent";

const sendRequest = ({method, endpoint, data = {}}) => {
    return new Promise((resolve, reject) => {
        // Removed leading and trailing slashes.
        endpoint = endpoint.replace(/^\//, '');
        endpoint = endpoint.replace(/\/$/, '');

        request[method](`api/${endpoint}`).send(data).end((err, res) => {
            if (err && !res.body) {
                reject(err);
                return;
            }

            resolve(res.body);
        });
    });
};

export default class Api {
    static get(endpoint, data = {}) {
        return sendRequest({method: 'get', endpoint, data})
    }

    static post(endpoint, data = {}) {
        return sendRequest({method: 'post', endpoint, data})
    }
}

import React, {Component} from "react";
import Payg from "./Payg/index";

class AppContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            value: '{"url": "http://www.payasugym.com"}',
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }


    handleSubmit(event) {
        let request = JSON.parse(this.state.value);

        if (typeof request.url != 'undefined') {
            var data = Payg.Api.post('/api.php', request);
            var promise = Promise.resolve(data);
            promise.then(function(value) {
                var responsestring = "API RESPONSE:  {\"url\": \"" + value.url + "\", \"short_url\":\"" + value.short_url + "\"}";
                alert(responsestring);
            });

        } else {
            alert('please provide correct json body i.e {"url": "http://www.payasugym.com"}');
            return false;
        }

        event.preventDefault();
    }

    render() {
        return (
            <div style={{height: '60vh', width: '70%', padding: '20px'}}>
                <h1>Url Shortner  - Test application for PAYG - Robert Czusz</h1>
            <form onSubmit={this.handleSubmit}>
                <div>
                <label>
                    Request body (JSON):
                </label>
                </div>
                <div>
                    <textarea type="text" value={this.state.value} onChange={this.handleChange} required={true} />
                </div>
                <input type="submit" value="Submit POST request body" />
            </form>
            </div>
        );
    }
}

export default AppContainer;
